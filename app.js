const express = require("express")
const parseArgs = require("minimist")

let args = parseArgs(process.argv.slice(2))
const port = args.port || 3000
console.log(args)
console.log(port)

const app = express()

app.get("/", (req, res) => res.send("Hello World!"))

app.listen(port, () => console.log("Example app listening on port ${post}!"))